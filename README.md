# The Vodafone Retrospective Moment
The Vodafone Retrospective Moment is a project made by [iDA Mediafoundry](https://www.weareida.digital/nl-en.html) for Vodafone to give Vodafone customers a retrospective moment of six months.  
The app uses customer data to give a customer insights about their subscription and what they can improve about this subscription, such as a cheaper price or a larger subscription.

## Requirements
- Make sure you have [Java 11.0.11](https://www.oracle.com/nl/java/technologies/javase-jdk11-downloads.html) installed on your device.
- This project depends on [Maven](https://maven.apache.org/) to build the project. [Install](https://maven.apache.org/download.cgi) Maven if you haven't already.
- This project uses [HubSpot](https://www.hubspot.com/) to retrieve data. First create an account and put the [hapikey](https://knowledge.hubspot.com/integrations/how-do-i-get-my-hubspot-api-key) in the application.properties or an argument when starting the app.

### Before you start
This is only the backend of the app.  
For the full app, you also need the Vue.js application which can be found [here](https://bitbucket.org/vanudro/vodafone-lookback.vue/src/master/).
If you're not familiar with Vue, please head to [their website](https://vuejs.org/) and learn more about this framework

## Project setup

### Running the project
To start the project after installation run:
```
mvn spring-boot:run
```

### Building the project
To build the project after installation, you can build the project using:
```
mvn package
```
This wil build a JAR file in the /target directory.  
**Important!** _Make sure to comment the SSL configuration if you don't have an SSL certificate._

## Running the project.
To execute the JAR file run:
```
java -jar vodafone-lookback-0.2.2-SNAPSHOT.jar --api.token=HUBSPOT_TOKEN --logging.level.root=INFO --spring.mail.psort=PORT --spring.mail.username=MAIL_USERNAME --spring.mail.password=MAIL_PASSWORD
```
_Or put the command in a `.sh` file and run it locally._

## Contact & disclosure
This code was developed as a graduation project at the start of Februari 2021 until the end of June 2021.  
This project belongs to [iDA Mediafoundry](https://www.weareida.digital/nl-en.html). If you want to know more, please contact me at [robin.vanuden@ida-mediafoundry.nl](mailto:robin.vanuden@ida-mediafoundry.nl) or the company directly [here](https://www.weareida.digital/nl-en/contact.html) .
