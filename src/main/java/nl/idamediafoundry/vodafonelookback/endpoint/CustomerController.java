package nl.idamediafoundry.vodafonelookback.endpoint;

import nl.idamediafoundry.vodafonelookback.data.Customers;
import nl.idamediafoundry.vodafonelookback.data.mail.MailManager;
import nl.idamediafoundry.vodafonelookback.data.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@CrossOrigin
@RequestMapping(path = "/customers", produces = "application/json")
public class CustomerController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @GetMapping(path = {"", "/"})
    public Collection<Customer> getAll() {
        return Customers.list();
    }

    @GetMapping(path = {"/{pin}", "/{pin}/"})
    public Customer getById(@PathVariable String pin) {
        return Customers.getByPin(pin);
    }

    @PostMapping(path = {"/mail/{pin}", "/mail/{pin}/"})
    public boolean sendMailToCustomer(@PathVariable String pin) {
        Customer customer = Customers.getByPin(pin);
        if (customer == null) {
            LOGGER.debug("Customer not found");
            return false;
        }
        return MailManager.send(customer.getEmail(), String.format("%s %s", customer.getFirstname(), customer.getLastname()), customer.getFirstname());
    }
}
