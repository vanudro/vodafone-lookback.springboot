package nl.idamediafoundry.vodafonelookback.endpoint;

import nl.idamediafoundry.vodafonelookback.data.Contracts;
import nl.idamediafoundry.vodafonelookback.data.model.Contract;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@CrossOrigin
@RequestMapping(path = "/contracts", produces = "application/json")
public class ContractController {

    @GetMapping(path = {"", "/"})
    public Collection<Contract> getAll() {
        return Contracts.list();
    }

    @GetMapping(path = {"/types", "/types/"})
    public Contracts.Type[] getTypes() {
        return Contracts.Type.values();
    }

    @GetMapping(path = {"/{type}", "/{type}/"})
    public Contract getById(@PathVariable Contracts.Type type) {
        return Contracts.getByType(type);
    }

}
