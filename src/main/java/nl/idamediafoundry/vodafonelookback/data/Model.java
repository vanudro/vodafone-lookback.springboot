package nl.idamediafoundry.vodafonelookback.data;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Model {

    // Turns new-line-separated values into String array
    protected static List<String> splitNewLines(String raw) {
        return raw != null ? Arrays.asList(raw.split("\n")) : null;
    }

    protected static List<Integer> splitNewLinesInt(String raw) {
        List<String> split = splitNewLines(raw);
        if (split != null) {
            List<Integer> lines = new ArrayList<>();
            for (String s : split) {
                lines.add(Integer.valueOf(s));
            }
            return lines;
        }
        return null;
    }
}
