package nl.idamediafoundry.vodafonelookback.data.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.client.RestTemplate;

import java.util.Properties;

@Configuration
public class MainConfiguration {

    @Bean // Load mail settings from properties/ arguments
    public JavaMailSender getJavaMailSender(@Value("${spring.mail.username}") String username, @Value("${spring.mail.password}") String password, @Value("${spring.mail.port}") int port, @Value("${spring.mail.properties.mail.smtp.auth}") boolean auth, @Value("${spring.mail.properties.mail.smtp.starttls.enable}") boolean tls, @Value("${spring.mail.properties.mail.smtp.ssl.enable}") boolean ssl) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", auth ? "true" : "false");
        props.put("mail.smtp.starttls.enable", tls ? "true" : "false");
        props.put("mail.smtp.ssl.enable", ssl ? "true" : "false");
        props.put("mail.debug", "true");
        // If SSL is enabled. Add socket factory for different mail port.
        if (ssl) {
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        }
        return mailSender;
    }
}
