package nl.idamediafoundry.vodafonelookback.data;

import nl.idamediafoundry.vodafonelookback.data.model.Contract;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Contracts {

    static Logger LOGGER = LoggerFactory.getLogger(Contracts.class);

    public enum Type {Red_Unlimited, Red_Together, Red, Start_L, Start_M}

    protected static Map<Type, Contract> contracts;

    public static void init() {
        contracts = new HashMap<>();

        // Predefine contracts from Vodafone
        // Source: (https://www.vodafone.nl/abonnement/mobiel/sim-only)
        contracts.put(Type.Red_Unlimited, new Contract(32.50, Contract.UNLIMITED, Contract.UNLIMITED));
        contracts.put(Type.Red_Together, new Contract(30, 50000, Contract.UNLIMITED, true));
        contracts.put(Type.Red, new Contract(25, 12500, Contract.UNLIMITED));
        contracts.put(Type.Start_L, new Contract(17.50, 4000, 150));
        contracts.put(Type.Start_M, new Contract(13.50, 1500, 150));
        LOGGER.info("Contracts set");
    }

    public static Contract getByType(Type type) {
        return contracts.get(type);
    }

    public static Contract get(String t) {
        if (t != null) {
            // Loop through all types
            for (Type type : Type.values()) {
                // Found type
                if (type.name().equals(t)) {
                    return getByType(type);
                }
            }
        }
        // Unknown type
        return null;
    }

    public static Collection<Contract> list() {
        return contracts.values();
    }
}
