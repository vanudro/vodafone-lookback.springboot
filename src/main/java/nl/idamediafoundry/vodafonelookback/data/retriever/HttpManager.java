package nl.idamediafoundry.vodafonelookback.data.retriever;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import nl.idamediafoundry.vodafonelookback.data.Contracts;
import nl.idamediafoundry.vodafonelookback.data.Customers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class HttpManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpManager.class);
    public static String API_ROOT;
    public static String HUBSPOT_TOKEN;

    private final String path;
    private final Map<String, String> parameters;

    @Autowired
    public HttpManager(@Value("${api.root}") String propApiRoot, @Value("${api.token}") String propHubspotToken) {
        this.parameters = new HashMap<>();
        this.path = "";
        // Checks to ensure that a API call to HubSpot can be made.
        // Errors will log if properties are missing.
        if (propApiRoot == null) {
            LOGGER.error("API_ROOT property was not loaded.");
            LOGGER.error("Application did NOT start...");
            return;
        }
        if (propHubspotToken == null) {
            LOGGER.error("HUBSPOT_TOKEN property was not loaded.");
            LOGGER.error("Application did NOT start...");
            return;
        }
        if (propHubspotToken.equals("TOKEN")) {
            LOGGER.error("HUBSPOT_TOKEN is not defined.");
            LOGGER.error("Application did NOT start...");
            return;
        }
        LOGGER.info("Loaded API: " + propApiRoot);
        HttpManager.API_ROOT = propApiRoot;
        HttpManager.HUBSPOT_TOKEN = propHubspotToken;
        Contracts.init();
        Customers.init();
    }

    public HttpManager(String path) {
        this.parameters = new HashMap<>();
        this.path = path;
    }

    public void addParameter(String key, String value) {
        this.parameters.put(key, value);
    }

    public String getParameters() {
        StringBuilder builder = new StringBuilder();
        // Get all key-values and merge them together
        for (Map.Entry<String, String> entry : this.parameters.entrySet()) {
            builder.append("&").append(entry.getKey()).append("=").append(entry.getValue());
        }
        return builder.toString();
    }

    public String load(String stringClass) {
        String url = String.format("%s/%s?hapikey=%s%s", HttpManager.API_ROOT, this.path, HttpManager.HUBSPOT_TOKEN, this.getParameters());

        try {
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.getForObject(url, stringClass.getClass());
        } catch (RestClientException e) {
            // Default exception if call cannot be made
            LOGGER.debug("Error while executing: " + url, e);
            return null;
        } catch (IllegalArgumentException e) {
            // Exception if call can be made but returns an error
            LOGGER.debug("Error with response from: " + url, e);
            return null;
        } catch (Exception e) {
            // Other errors
            LOGGER.error("Error: ", e);
            return null;
        }
    }

    public JsonObject load() {
        String raw = this.load("");
        if (raw != null) {
            try {
                return JsonParser.parseString(raw).getAsJsonObject();
            } catch (JsonSyntaxException e) {
                // Default error when load() returns non-JSON format
                LOGGER.error("Syntax error: ", e);
                return new JsonObject();
            } catch (Exception e) {
                // Other errors
                LOGGER.error("Error: ", e);
                return new JsonObject();
            }
        }
        return null;
    }

}
