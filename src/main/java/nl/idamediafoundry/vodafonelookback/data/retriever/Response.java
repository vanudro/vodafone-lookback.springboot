package nl.idamediafoundry.vodafonelookback.data.retriever;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Response {

    private String id;
    private JsonObject properties;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("updated_at")
    private String updatedAt;
    private boolean archived;

    public JsonObject getHubSpotJson(){
        JsonObject obj = this.getProperties();
        obj.addProperty("id", this.getId());
        obj.addProperty("created_at", this.getCreatedAt());
        obj.addProperty("updated_at", this.getUpdatedAt());
        return obj;
    }
}
