package nl.idamediafoundry.vodafonelookback.data.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import nl.idamediafoundry.vodafonelookback.data.Model;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Getter
public class Contract extends Model {

    public static final int UNLIMITED = 999;

    protected double priceMonth;
    protected int mbAmount;
    protected int callingMinutes;
    protected boolean shareData;

    public Contract(double priceMonth, int mbAmount, int callingMinutes) {
        this(priceMonth, mbAmount, callingMinutes, false);
    }

    public Contract(double priceMonth, int mbAmount, int callingMinutes, boolean shareData) {
        this.priceMonth = priceMonth;
        this.mbAmount = mbAmount;
        this.callingMinutes = callingMinutes;
        this.shareData = shareData;
    }
}
