package nl.idamediafoundry.vodafonelookback.data.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import nl.idamediafoundry.vodafonelookback.data.Contracts;
import nl.idamediafoundry.vodafonelookback.data.HubSpotModel;

import java.util.List;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Customer extends HubSpotModel {

    private String firstname, lastname;
    private String email, phone;
    private String pin;
    @SerializedName("contract_type")
    private String contractType;
    @SerializedName("contract_date")
    private String contractDate;
    @SerializedName("contract_data_used")
    private String contractDataUsed;
    @SerializedName("contract_calling_minutes_used")
    private String contractCallingMinutesUsed;
    @SerializedName("contract_duur")
    private int contractLength;
    @SerializedName("new_contract_type")
    private String newContractType;
    @SerializedName("app_installed")
    private boolean appInstalled;

    List<Integer> contractData, contractCallingMinutes;
    private Contract contract;

    public static Customer fromResponse(JsonElement element) throws JsonSyntaxException {
        // Create a customer object from Response
        Customer customer = fromResponse(element, Customer.class);
        // Set pincode from id by adding zero's and take the first 4 characters
        customer.setPin(String.format("%s0000", customer.getId()).substring(0, 4));
        // Set contract from type
        customer.setContract(Contracts.get(customer.getContractType()));
        // Convert new-line-separated values into int lists.
        customer.setContractData(splitNewLinesInt(customer.getContractDataUsed()));
        customer.setContractCallingMinutes(splitNewLinesInt(customer.getContractCallingMinutesUsed()));
        return customer;
    }
}
