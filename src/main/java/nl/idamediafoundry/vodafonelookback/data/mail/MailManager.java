package nl.idamediafoundry.vodafonelookback.data.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Component
public class MailManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailManager.class);
    private static String username;
    private static JavaMailSender sender;
    private static TemplateEngine engine;

    @Autowired // Loads mail username (email address) from properties/ arguments
    MailManager(JavaMailSender sender, TemplateEngine engine, @Value("${spring.mail.username}") String username) {
        MailManager.sender = sender;
        MailManager.engine = engine;
        MailManager.username = username;
        LOGGER.info("init mail service");
    }

    public static String getTemplate(String name) {
        Context context = new Context();
        // Pass variable to the template
        context.setVariable("name", name);
        // Return template as string with passed variables
        return engine.process("lookback", context);
    }

    public static boolean send(String toAddress, String toName, String name) {
        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            LOGGER.info("Sending mail from " + username + " to " + toAddress);
            // Set sender and receiver information
            helper.setFrom(new InternetAddress(username, "Team Vodafone"));
            helper.setTo(new InternetAddress(toAddress, toName));
            helper.setText(getTemplate(name), true);

            // Subject
            helper.setSubject("Jouw abonnement in een paar minuten. ⏱️");

            // Sending mail
            sender.send(message);
            return true;
        } catch (MessagingException e) {
            // Special catch when mail won't send
            LOGGER.error("Mail error", e);
        } catch (Exception e) {
            LOGGER.debug("Unhandled error", e);
        }
        return false;
    }

    // Used to send a basic mail without HTML
    public static void sendDefault(String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("noreply@robinvanuden.com");
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        sender.send(message);
        LOGGER.info("Send mail#" + subject + " to " + to);
    }
}
