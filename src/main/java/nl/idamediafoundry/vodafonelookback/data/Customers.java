package nl.idamediafoundry.vodafonelookback.data;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import nl.idamediafoundry.vodafonelookback.data.model.Customer;
import nl.idamediafoundry.vodafonelookback.data.retriever.HttpManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Customers {

    static Logger LOGGER = LoggerFactory.getLogger(Customers.class);
    static Map<String, Customer> customers;

    public static void init() {
        customers = new HashMap<>();
        HttpManager importer = new HttpManager("objects/contacts");
        // Properties to return from HubSpot API to fill Customer object
        importer.addParameter("properties", "firstname,lastname,email,phone,contract_type,contract_date,contract_duur,contract_data_used,contract_calling_minutes_used,new_contract_type,app_installed");
        // Load data from API
        JsonObject raw_data = importer.load();
        if (raw_data != null) {
            if (raw_data.has("results")) {
                JsonArray array = raw_data.get("results").getAsJsonArray();
                for (JsonElement element : array) {
                    // Turn JsonElement into Customer object and add to Map
                    Customer customer = Customer.fromResponse(element);
                    customers.put(customer.getPin(), customer);
                }
            }
            LOGGER.info("Customers loaded in");
        } else {
            LOGGER.error("Customers NOT loaded in");
        }
    }

    public static Collection<Customer> list() {
        return customers.values();
    }

    public static Customer getByPin(String pin) {
        return customers.get(pin);
    }
}
