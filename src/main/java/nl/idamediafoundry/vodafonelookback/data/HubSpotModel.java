package nl.idamediafoundry.vodafonelookback.data;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import lombok.Getter;
import nl.idamediafoundry.vodafonelookback.data.retriever.Response;

@Getter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class HubSpotModel extends Model {

    private String id;

    // Turns JSON into a Response
    public static Response toResponse(JsonElement element) {
        // Uses class to convert object into desired object
        Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        return gson.fromJson(element.toString(), Response.class);
    }

    // Turns a response to a defined object
    public static <T> T fromResponse(Response response, Class<T> c) {
        // Uses class to convert object into desired object
        return new Gson().fromJson(response.getHubSpotJson(), c);
    }

    // Turns JSON to a defined object
    public static <T> T fromResponse(JsonElement element, Class<T> c) {
        return fromResponse(toResponse(element), c);
    }

}
